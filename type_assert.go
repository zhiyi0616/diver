package main

/*
 * There is nothing like "template" in golang
 * This function is to convert []interface{} to []string
 */
func SliceString(interfaceSlice interface{}) []string {
	interfaceSlice_ := interfaceSlice.([]interface{})
	len_ := len(interfaceSlice_)
	outSlice := make([]string, len_)
	for id, v := range interfaceSlice_ {
		outSlice[id] = v.(string)
	}
	return outSlice
}
