/*\
* *  # PROJECT/PROGRAM
* *  This project is a netlist propagation simulator
* *  Written by Yusang Luo (luoyusang2007@hotmail.com)
* *
* *  # FILE
* *  This file implements a simple queue manager for the scheduling of
* *  netlist signal propagation.
* *
* *  Jan 2021
\*/

package main

// Wrapper for CircularQueue
type NodePropQueue struct {
	CircularQueue
}

// Put node in queue and set scheduled flag
func (self *NodePropQueue) Offer(val NodeIface) {
	if !self.CircularQueue.Offer(val) {
		panic("Offer-in failed.")
	}
	val.SetScheduledFlag()
}

// Poll out node
func (self *NodePropQueue) Poll() (val NodeIface, ok bool) {
	val_, ok := self.CircularQueue.Poll()
	if !ok {
		panic("Poll-out failed.")
	}
	val = val_.(NodeIface)
	val.ResetScheduledFlag()
	return val, ok
}

// Wrapper of the constructor
func newNodePropQueue(maxLen int) *NodePropQueue {
	return &NodePropQueue{
		CircularQueue{
			head:   0,
			tail:   0,
			slice_: make([]interface{}, maxLen+1),
		},
	}
}
