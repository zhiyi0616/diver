package main

type EventNode struct {
	NetlistNode
}

func (self *EventNode) Eval() Vec {
	return self.InputVals[0]
}
