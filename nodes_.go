package main

import (
	"fmt"
	"strconv"
	"sync"
)

// Stored as output in each nodes
type NodeInput struct {
	TargetNode NodeIface
	PortIndex  int
}

// All nodes
type NodeIface interface {
	Eval() Vec                     // Calculate the output value
	AddOutNode(int, NodeIface)     // [Initialize] Attach output
	SetInputVal(int, Vec)          // [Unsafe] Set Input value without any check, dangerous.
	GetInputVals() []Vec           // Get the input value list
	GetOutNodeInputs() []NodeInput // Get all output targets
	GetOutWidth() int              // [Initialize] Get output width
	GetLabelStr() string           // [Debug] Get output width
	SetScheduledFlag()             // [Schedule]
	GetScheduledFlag() bool        // [Schedule] Check if is scheduled.
	ResetScheduledFlag()           // [Schedule]
	Receive(int, Vec) (bool, bool) // Check if any change, set value
}

// Node Basic
type NetlistNode struct {
	FuncType
	InputVals     []Vec       // The input values of a functor
	OutNodeInputs []NodeInput // All target NetlistNodes (receivers of my output)

	LabelStr string // VVP label for debug
	OutWidth int    // For receiver to determine their input width REMOVEME: remove when optimized.
}

// Add out node This function is called during connecting nodes.
func (self *NetlistNode) AddOutNode(outerPortIndex int, outFun NodeIface) {
	ni := NodeInput{
		TargetNode: outFun,
		PortIndex:  outerPortIndex,
	}
	self.OutNodeInputs = append(self.OutNodeInputs, ni)
}

// This function will set the input value.
// No copying will be processed.
func (self *NetlistNode) SetInputVal(selfPortIndex int, val Vec) {
	if selfPortIndex >= len(self.InputVals) {
		panic("Index out of range.")
	} else {
		self.InputVals[selfPortIndex] = val
	}
}

func (self *NetlistNode) GetInputVals() []Vec {
	return self.InputVals
}

func (self *NetlistNode) GetOutNodeInputs() []NodeInput {
	return self.OutNodeInputs
}

func (self *NetlistNode) GetOutWidth() int {
	return self.OutWidth
}
func (self *NetlistNode) GetLabelStr() string {
	return self.LabelStr
}

func (self *NetlistNode) SetScheduledFlag() {
	return
}

func (self *NetlistNode) GetScheduledFlag() bool {
	return false
}

func (self *NetlistNode) ResetScheduledFlag() {
	return // Generic nodes do not have scheduled flag.
}

/*
in golang, a slice is always 24bytes. and array's size is variable with length.
*/
func (self *NetlistNode) Eval() Vec {
	panic("Pure Virtual Function.")
}

// 关于 SchedulableNode中 锁的知识可以参考说明文档对golang锁的简介
type SchedulableNode struct {
	NetlistNode
	ValLock      sync.RWMutex // Protect Input vals
	ScheduleLock sync.RWMutex // Protect IsScheduled
	IsScheduled  bool         // If scheduled.
}

func (self *SchedulableNode) SetScheduledFlag() {
	GDebugDumpBuf.Write([]byte(fmt.Sprintf("%s SetScheduled\n", self.GetLabelStr())))
	GDebugDumpBuf.Flush()
	self.IsScheduled = true
}

func (self *SchedulableNode) GetScheduledFlag() bool {
	return self.IsScheduled
}
func (self *SchedulableNode) ResetScheduledFlag() {
	GDebugDumpBuf.Write([]byte(fmt.Sprintf("%s ResetScheduled\n", self.GetLabelStr())))
	GDebugDumpBuf.Flush()
	self.IsScheduled = false
}

func (self *NetlistNode) Receive(portIndex int, newVal Vec) (needSchedule bool, needSend bool) {
	if self.InputVals[portIndex].eeq(newVal) {
		return false, false
	}
	self.SetInputVal(portIndex, newVal)
	return false, true
}

func (self *SchedulableNode) Receive(portIndex int, newVal Vec) (needSchedule bool, needSend bool) {
	if self.InputVals[portIndex].eeq(newVal) {
		return false, false
	}
	self.SetInputVal(portIndex, newVal)
	if !self.IsScheduled {
		return true, false
	}
	return false, false
}

/*
 * To eval and send-out the output of a NetlistNode
 */
func EvalAndSend(fromNode NodeIface) {
	newOutVal := fromNode.Eval()
	// Resetting schedule flag is done by polling
	var newVal Vec

	/*
	* dump_signal输入的格式为： 传入结点Label[传入结点地址] = 传入结点Eval()后的输出
	* [%p] 中是地址 ，因为队列当中的存储返回的都是 return&
	 */
	GSignalDumpBuf.Write([]byte(fmt.Sprintf("%s[%p]=%v\n", fromNode.GetLabelStr(), fromNode, newOutVal.toString())))

	/*
	*  获取本处理结点的所有 输出目标结点 注意返回的是
	*  OutNodeInputs []NodeInput 中的 []NodeInput
	* 每一个遍历的元素是 NodeInput 就是每一个该结点的输出
	 */
	for _, outFunInput := range fromNode.GetOutNodeInputs() {
		targetNode := outFunInput.TargetNode
		// oldVal 是对于targetNode的 来自原处理结点的输入值 即 fromNode 对于其输出结点的输出值
		oldVal := targetNode.GetInputVals()[outFunInput.PortIndex]

		if fromPartialDriver, ok := fromNode.(*PartDriverNode); ok { // If node is part-driving node
			println("Warning: partial drive is not under full test")
			newVal = oldVal.combine(newOutVal, fromPartialDriver.OutWidth-fromPartialDriver.TargetPortOffset-fromPartialDriver.DriveWidth)
		} else { // If node is not part-driving node
			newVal = newOutVal
		}

		needSchedule, needSend := targetNode.Receive(outFunInput.PortIndex, newVal)
		if needSchedule {
			GScheduleQueue.Offer(targetNode) // Global var GScheduleQueue
		} else if needSend {
			EvalAndSend(targetNode) // Recursive
		}

	}
}

func CreateNetlistNode(jsonNode map[string]interface{}, label string) NodeIface {
	type_ := MakeFuncType(jsonNode["type"].(string))
	inputs := jsonNode["inputs"].([]interface{})
	params := jsonNode["params"].(map[string]interface{})
	node := NetlistNode{
		FuncType:      type_,
		OutNodeInputs: make([]NodeInput, 0, 32), // FIXME... 32
		InputVals:     make([]Vec, len(inputs)),
		OutWidth:      int(jsonNode["width"].(float64)),
		LabelStr:      label,
	}

	/*  进队列的意思是等一下再访问，目的是降低时间复杂度
	 一般单输入的结点不入
	 	有isscheduled参数的节点都是通过队列进行求解的入队前检查
	如果发现是false，不入的 		*/

	schedulableNode := SchedulableNode{
		NetlistNode: node,
		IsScheduled: false,
	}
	switch type_ {
	case N_ABS:
		return &FuncArithSingleInNode{
			NetlistNode: node,
		}
	case N_ARITH_POW, N_ARITH_MULT, N_ARITH_DIV, N_ARITH_SUB, N_ARITH_SUM:
		return &FuncArithDoubleInNode{
			SchedulableNode: schedulableNode,
		}
	case N_CMP_EEQ, N_CMP_EQ, N_CMP_NEE, N_CMP_NE:
		return &CompareNode{
			NetlistNode: node,
		}
	case N_EVENT_ANYEDGE, N_EVENT_POSEDGE, N_EVENT_NEGEDGE, N_EVENT_OR:
		return &EventNode{
			NetlistNode: node,
		}
	case N_FUNC_BUF, N_FUNC_BUFT, N_FUNC_BUFZ, N_FUNC_NOT:
		return &FuncLogicSingleInNode{
			NetlistNode: node,
		}
	case N_FUNC_OR, N_FUNC_AND, N_FUNC_NAND, N_FUNC_NOR, N_FUNC_XNOR, N_FUNC_XOR:
		return &FuncLogicSchedulableNode{
			SchedulableNode: schedulableNode,
		}
	case N_FUNC_MUXZ:
		return &FuncMuxFrom2Node{
			SchedulableNode: schedulableNode,
		}
	case N_VAR:
		return &VarNode{
			NetlistNode: node,
		}
	case N_UDP:
		defBodyP := GUDPDefMap[params["def_type"].(string)]
		if defBodyP.IsSequential() {
			inputEdges := make([]Edge, len(inputs))
			// Init edge values
			for i := range inputEdges {
				inputEdges[i] = E_NC
			}
			return &FuncSeqUDPNode{
				NetlistNode: node,
				DefBodyP:    defBodyP.(*SeqUDPDef),
				InputEdges:  inputEdges,
				OutVal:      defBodyP.GetInitVal(),
			}
		} else {
			return &FuncCombUDPNode{
				NetlistNode: node,
				DefBodyP:    defBodyP.(*CombUDPDef),
			}
		}
	case N_DELAY: // To be supported
		return &DelayNode{
			NetlistNode: node,
		}
	case N_CONCAT, N_CONCAT8:
		return &ConcatNode{
			NetlistNode: node,
		}
	case N_VPART_SEL:
		offset, _ := strconv.Atoi(params["base"].(string))
		return &PartSelectNode{
			NetlistNode:         node,
			SourceOffsetFromLsb: offset,
		}
	case N_VPART_DRIVER:
		return &PartDriverNode{
			NetlistNode:      node,
			TargetPortOffset: params["target_port_offset"].(int), // FIXME: Analyzer is not supported yet
			DriveWidth:       params["drive_width"].(int),
		}
	case N_FUNC_BUFIF1, N_FUNC_BUFIF0:
		return &BufifNode{
			NetlistNode: node,
		}
	case N_FUNC_PMOS, N_FUNC_NMOS, N_FUNC_RNMOS, N_FUNC_RPMOS, N_FUNC_CMOS, N_FUNC_RCMOS:
		return &MosNode{
			NetlistNode: node,
		}
	default:
		println("Node type:", type_)
		panic("Unsupported node type.")
	}
}
