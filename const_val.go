package main

import (
	"errors"
	"strconv"
	"strings"
)

func InputIsConst(inputStr string) bool {
	strlen := len(inputStr)
	if strlen < 4 {
		return false // not const
	} else if inputStr[0] != 'C' {
		return false
	} else if strings.Contains(inputStr, "<") {
		if inputStr[strlen-1] == '>' {
			return true // is const
		} else {
			println("ERR: Unknown type of input..")
			return false
		}
	}
	return false

}

func EvalConst(inputStr string) (Vec, error) {
	strlen := len(inputStr)
	res := Vec{}
	// Check if is a int
	i, err := strconv.ParseInt(inputStr, 0, 64)
	if err != nil {
		println("Evaluating via int64", i)
		return MakeVecViaU64(uint64(i), 64), nil // FIXME: 64
	}

	// Check if is a CX<>
	if strlen <= 4 {
		return nil, errors.New("...")
	}
	if inputStr[0] != 'C' {
		return nil, errors.New("...")
	}
	if inputStr[2] != '<' || inputStr[strlen-1] != '>' {
		return nil, errors.New("...")
	}
	switch inputStr[1] {
	case '4':
	case '8':
	case 'r':
	default:
		return nil, errors.New("Unsupported const type.")
	}
	println("Evaluating via string", inputStr[3:strlen-1])
	res = MakeVecViaStr(inputStr[3 : strlen-1])
	return res, nil
}
