package main

type BufifNode struct {
	NetlistNode
}

func (self *BufifNode) Eval() Vec {
	selfLen := len(self.InputVals[0])
	if len(self.InputVals[0]) != len(self.InputVals[1]) {
		panic("length not equal !!")
	}
	res := make(Vec, selfLen)
	for i := 0; i < selfLen; i++ {
		res[i] = self.InputVals[0][i].Logicbufif(self.InputVals[1][i], self.FuncType)
	}
	return res
}

// data: 0  control: 1
func (self Bit) Logicbufif(controlSignal Bit, nodetype FuncType) Bit {
	inputCategory := self.ToFormal()
	controlCategory := controlSignal.ToFormal()
	switch controlCategory {
	case VAL_0_FORMAL:
		if nodetype == N_FUNC_BUFIF0 {
			if inputCategory == VAL_Z_FORMAL {
				return VAL_X_FORMAL
			} else {
				return self
			}
		} else {
			return VAL_Z_FORMAL
		}
	case VAL_1_FORMAL:
		if nodetype == N_FUNC_BUFIF0 {
			return VAL_Z_FORMAL
		} else {
			if inputCategory == VAL_Z_FORMAL {
				return VAL_X_FORMAL
			} else {
				return self
			}
		}
	case VAL_X_FORMAL, VAL_Z_FORMAL:
		if VAL_0_FORMAL == inputCategory || VAL_1_FORMAL == inputCategory {
			return self.ToSingalLH()
		} else {
			return VAL_X_FORMAL
		}
	default:
		panic("err")
	}
}
