package main

import (
	"bufio"
	"bytes"
	"encoding/json"
	"flag"
	"fmt"
	"io/ioutil"
	"log"
	"net"
	"os"
	"strings"
	"sync"
	"time"

	"net/http"         // For performance statistic
	_ "net/http/pprof" // For performance statistic
)

// The args
var GFlagInputFile string
var GFlagServeAddr string

var GDebugDumpBuf *bufio.Writer
var GSignalDumpBuf *bufio.Writer

var GScheduleQueue = newNodePropQueue(10000)
var GNodeMap = make(map[string]NodeIface)
var GSignalMap = make(map[string]SignalIface)
var GUDPDefMap = make(map[string]UDPDefIface)

var GWaitGroup sync.WaitGroup

func flagInit() {
	flag.StringVar(&GFlagInputFile, "file-name", "adder_32.json", "The input netlist file.")
	flag.StringVar(&GFlagServeAddr, "cmd-serve-addr", ":8787", "UDP Command Service Address.")
	flag.Parse()
}

func main() {
	flagInit()                          // Arg parsing
	var jsonRead map[string]interface{} // Json container

	go func() {
		log.Println(http.ListenAndServe(":8084", nil)) // Open http://127.0.0.1:8084/debug/pprof/ to see performance statistic
	}()

	signalDumpFile_, err := os.OpenFile("dump_signal.dump", os.O_WRONLY|os.O_CREATE|os.O_TRUNC, 0666)
	if err != nil {
		panic("Failed to open dump file.")
	}
	GSignalDumpBuf = bufio.NewWriter(signalDumpFile_)
	debugDumpFile_, err := os.OpenFile("dump_debug.dump", os.O_WRONLY|os.O_CREATE|os.O_TRUNC, 0666)
	if err != nil {
		panic("Failed to open dump file.")
	}
	GDebugDumpBuf = bufio.NewWriter(debugDumpFile_)

	netlistJsonStr, err := ioutil.ReadFile(GFlagInputFile)
	if err != nil {
		fmt.Println("Err reading file.", GFlagInputFile)
		return
	}
	json.Unmarshal(netlistJsonStr, &jsonRead)

	// UDP Def map should be built before Nodes
	jsonUDPDefMap := jsonRead["udp_definitions"].(map[string]interface{})
	fmt.Println("Creating UDP def map...")
	for UDPDefLabel := range jsonUDPDefMap {
		jsonUDPDef := jsonUDPDefMap[UDPDefLabel].(map[string]interface{})
		GUDPDefMap[UDPDefLabel] = CreateUDPDef(jsonUDPDef, UDPDefLabel)
	}

	// Build logic node map
	jsonNodesMap := jsonRead["nodes"].(map[string]interface{})
	fmt.Println("Creating node map...")
	for nodeLabel := range jsonNodesMap {
		jsonNode := jsonNodesMap[nodeLabel].(map[string]interface{})
		GNodeMap[nodeLabel] = CreateNetlistNode(jsonNode, nodeLabel)
	}

	// Build signal map
	jsonSignalMap := jsonRead["signals"].(map[string]interface{})
	fmt.Println("Creating signals map...")
	for signalLabel := range jsonSignalMap {
		jsonSignal := jsonSignalMap[signalLabel].(map[string]interface{})
		GSignalMap[signalLabel] = CreateSignal(jsonSignal, signalLabel)
	}

	// Setup the inputs, do the interconnection
	fmt.Println("Setting up inputs of nodes...")
	for nodeLabel := range jsonNodesMap {
		jsonNode := jsonNodesMap[nodeLabel].(map[string]interface{})
		netNode_i := GNodeMap[nodeLabel] //self
		jsonInputs := jsonNode["inputs"].([]interface{})

		for inputIndex := range jsonInputs {
			input := jsonInputs[inputIndex].(map[string]interface{})
			inputType := input["val_type"].(string)
			inputStr := input["val_str"].(string)
			if inputType == "NODE_LABEL" {
				// initial value x
				driverNode_i := GNodeMap[inputStr]
				driverNode_i.AddOutNode(int(inputIndex), netNode_i)
				netNode_i.SetInputVal(
					inputIndex,
					MakeVecViaCharRepeating('x', driverNode_i.GetOutWidth()),
				)
			} else if inputType == "CONST_VEC4" { // 如果是常量则则用原来常量的数值
				netNode_i.SetInputVal(
					inputIndex,
					MakeVecViaStr(inputStr),
				)
				// Here we propagate both unscheduled and no-need-to-schedule nodes.
				if !netNode_i.GetScheduledFlag() {
					GScheduleQueue.Offer(netNode_i)
				}
			} else {
				println("ERR: Input type not supported:", inputType)
				panic("Unsupported input.")
			}
		}
	}

	// Initial propagation
	fmt.Println("Doing Initial prop...")
	for GScheduleQueue.GetCurrentLen() > 0 {
		node, _ := GScheduleQueue.Poll()

		EvalAndSend(node)
		GSignalDumpBuf.Flush()
	}

	var signalIO []chan int
	GWaitGroup.Add(2)
	signalIO = make([]chan int, 2)
	for chanId, _ := range signalIO {
		go simWorker(chanId, signalIO)
	}
	go NetCmdHandlerViaTcp(GNodeMap, GSignalDumpBuf) // Start the command handler
	GWaitGroup.Wait()

}

// The worker routine, FIXME: Not finished
func simWorker(id int, signalIO []chan int) {
	var simTimeQueue *TimeStep
	var currentTime int64
	time.Sleep(2 * time.Second)

	signal := <-signalIO[id]
	if signal == 3 {
		currentTime++
		print(simTimeQueue)
	}

	var a = new(TimeStep)
	println("unCasual RT...", id, a)
	GWaitGroup.Done()
}

func NetCmdHandler(GNodeMap map[string]NodeIface, GSignalDumpBuf *bufio.Writer) {
	println("[UDP] listening at", GFlagServeAddr)
	addr, err := net.ResolveUDPAddr("udp", GFlagServeAddr) // 创建监听地址，这个服务端
	if err != nil {
		fmt.Println(err)
		os.Exit(1)
	}

	conn, err := net.ListenUDP("udp", addr)
	if err != nil {
		fmt.Println(err)
		os.Exit(1)
	}
	defer conn.Close()

	for {
		// Here must use make and give the length of buffer
		UDPDataBuf := make([]byte, 2048)
		amount, rAddr, err := conn.ReadFromUDP(UDPDataBuf)
		if err != nil {
			fmt.Println(err)
			continue
		}
		recvdData := UDPDataBuf[0:amount]
		recvdString := string(recvdData)
		splited := strings.Split(recvdString, "=")
		nodeLabel := splited[0]
		newVal := MakeVecViaStr(splited[1])
		targetNode := GNodeMap[nodeLabel]
		targetNode.SetInputVal(0, newVal)

		GScheduleQueue.Offer(targetNode)
		for GScheduleQueue.GetCurrentLen() > 0 {
			node, _ := GScheduleQueue.Poll()
			EvalAndSend(node)
			GSignalDumpBuf.Flush()
		}
		GSignalDumpBuf.Flush()

		_, err = conn.WriteToUDP([]byte("PROP_FINISHED"), rAddr) // 告诉客户端， 输入激励后网表传播完成
		if err != nil {
			fmt.Println(err)
			continue
		}
	}
}

func NetCmdHandlerViaTcp(GNodeMap map[string]NodeIface, GSignalDumpBuf *bufio.Writer) {
	println("[TCP] listening at", GFlagServeAddr)
	addr, err := net.ResolveTCPAddr("tcp", GFlagServeAddr)
	if err != nil {
		fmt.Println(err)
		os.Exit(1)
	}
	tcplisten, err := net.ListenTCP("tcp", addr)
	if err != nil {
		fmt.Println(err)
		os.Exit(1)
	}
	defer tcplisten.Close()
	conn, err := tcplisten.Accept()
	for {
		if err != nil {
			continue
		}
		// Here must use make and give the length of buffer
		TCPDataBuf := make([]byte, 2048)
		amount, err := conn.Read(TCPDataBuf)
		if err != nil {
			fmt.Println(err)
			break
		}
		recvTCPData := TCPDataBuf[0:amount]
		receivdatas := bytes.Split(recvTCPData, []byte("\n"))
		for i := 0; i < len(receivdatas); i++ {
			recvdData := receivdatas[i]
			if 0 == len(recvdData) {
				i++
				continue
			}
			recvdString := string(recvdData)
			splited := strings.Split(recvdString, "=")
			nodeLabel := splited[0]
			newVal := MakeVecViaStr(splited[1])
			targetNode := GNodeMap[nodeLabel]
			targetNode.SetInputVal(0, newVal)

			GScheduleQueue.Offer(targetNode)
			for GScheduleQueue.GetCurrentLen() > 0 {
				node, _ := GScheduleQueue.Poll()
				EvalAndSend(node)
				GSignalDumpBuf.Flush()
			}
		}
		GSignalDumpBuf.Flush()
		_, err = conn.Write([]byte("PROP_FINISHED"))
		if err != nil {
			fmt.Println(err)
			continue
		}
	}
}
