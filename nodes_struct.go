package main

type DelayNode struct {
	NetlistNode
}

func (self *DelayNode) Eval() Vec {
	return self.InputVals[0]
}

type ConcatNode struct {
	NetlistNode
}

func (self *ConcatNode) Eval() Vec {
	res := make(Vec, self.OutWidth)
	start_index := len(self.InputVals) - 1
	offset := 0

	res = res.combine(self.InputVals[start_index], offset) //FIXME: from right
	offset += len(self.InputVals[start_index])
	for i := start_index - 1; i >= 0; i-- { // Right-to-left
		res = res.combine(self.InputVals[i], offset) // SAFE.  combine is not in-situ //FIXME: from right
		offset += len(self.InputVals[i])
	}
	// DEBUG
	if len(res) != self.OutWidth {
		panic("Logic error... length changed.")
	}
	return res
}

type PartSelectNode struct {
	NetlistNode
	SourceOffsetFromLsb int // Source Offset from LSB (right-hand)
}

func (self *PartSelectNode) Eval() Vec {
	start := len(self.InputVals[0]) - (self.OutWidth + self.SourceOffsetFromLsb)
	end := start + self.OutWidth
	res := make(Vec, self.OutWidth)
	copy(res, self.InputVals[0][start:end])
	return res
}

// FIXME: Not finished
type PartDriverNode struct {
	NetlistNode
	TargetPortOffset int // TargetPortOffset
	DriveWidth       int
}

func (self *PartDriverNode) Eval() Vec {
	return self.InputVals[0]
}

// func (self *PartDriverNode) EvalAndSend() {
// 	newOutVal := self.Eval()
// 	GSignalDumpBuf.Write([]byte(fmt.Sprintf("%s[%p]=%v\n", self.LabelStr, self, newOutVal.toString())))
// 	for _, outFunInput := range self.OutNodeInputs {
// 		targetNode := outFunInput.TargetNode
// 		oldVal := targetNode.GetInputVals()[outFunInput.PortIndex]
// 		newSetVal := oldVal.combine(newOutVal, self.OutWidth-self.TargetPortOffset-self.DriveWidth)
// 		if oldVal.eeq(newSetVal) {
// 			continue
// 		}
// 		targetNode.Receive(outFunInput.PortIndex, newOutVal)
// 	}
// }
