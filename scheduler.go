package main

type Event struct {
	Next *Event
}

// Enum QueueType
type QueueType uint8

const (
	QUEUE_ACTIVE   QueueType = 11
	QUEUE_INACTIVE QueueType = 12
	QUEUE_NBASIGN  QueueType = 13
	QUEUE_MONITOR  QueueType = 14
)

type TimeStep struct {
	SimTime float64 // Global simulation time
	Next    *TimeStep

	// The queues
	QueueActive   *Event
	QueueInactive *Event
	QueueNbAssign *Event
	QueueMonitor  *Event

	// Indicates the end of the queues
	EndActive   *Event
	EndInactive *Event
	EndNbAssign *Event
	EndMonitor  *Event
}

/*
 * This function is to add(put) a event into the event queue of acertain Timestep.
 */
func (Ts *TimeStep) AddEvent(event_ *Event, queueType QueueType) {
	var ptr *Event = nil
	switch queueType {
	case QUEUE_ACTIVE:
		ptr = Ts.EndActive
	case QUEUE_INACTIVE:
		ptr = Ts.EndInactive
	case QUEUE_NBASIGN:
		ptr = Ts.EndNbAssign
	case QUEUE_MONITOR:
		ptr = Ts.EndMonitor
	}
	ptr.Next = event_
	event_.Next = nil
}

func RunEvent(Ts *TimeStep) {
	// Run the first event of current timestep

}

/*
 * This function is to schedule a event into the event queue
 * of a certain Timestep.
 */
func ScheduleEvent(fromEvent *Event, queueType QueueType, delay float64) {
	for fromEvent != nil {

	}
}
