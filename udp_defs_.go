package main

type Edge uint8

const (
	E_10 Edge = iota
	E_1X
	E_X0
	E_X1
	E_01
	E_0X
	E_NC // No change
)

type UDPDefIface interface {
	IsSequential() bool
	GetInitVal() Vec
}

type UDPDef struct {
	Name        string
	InputAmount int
	Table       []string
}

type CombUDPDef struct {
	UDPDef
}

func (self *CombUDPDef) IsSequential() bool {
	return false
}
func (self *CombUDPDef) GetInitVal() Vec {
	return Vec{}
}

func (self *CombUDPDef) CombEval(inputVals []Vec) Vec {
	for _, patternStr := range self.Table {
		patternStrBytes := []byte(patternStr)
		inputPatternStr := patternStrBytes[0 : len(patternStrBytes)-1]
		outVal := patternStrBytes[len(patternStrBytes)-1]
		lineAnyMismatch := false
		for i, patternChar := range inputPatternStr {
			if !valMatch(inputVals[i][0], patternChar) {
				lineAnyMismatch = true
				break
			}
		}
		if !lineAnyMismatch { // All match
			switch outVal {
			case '0':
				return Vec{VAL_0_FORMAL}
			case '1':
				return Vec{VAL_1_FORMAL}
			case 'x', 'X':
				return Vec{VAL_X_FORMAL}
			case 'z', 'Z':
				return Vec{VAL_Z_FORMAL}
			default:
				panic("ERR")
			}
		}
	}
	return Vec{VAL_X_FORMAL}
}

type SeqUDPDef struct {
	UDPDef
	InitVal Vec
}

func (self *SeqUDPDef) IsSequential() bool {
	return true
}
func (self *SeqUDPDef) GetInitVal() Vec {
	return self.InitVal
}

func (self *SeqUDPDef) SeqEval(inputVals []Vec, inputEdges []Edge, oldOut Vec) Vec {
	for _, patternStr := range self.Table {
		patternStrBytes := []byte(patternStr)
		inputPatternStr := patternStrBytes[1 : len(patternStrBytes)-1]
		oldOutPatternChar := patternStrBytes[0]
		outVal := patternStrBytes[len(patternStrBytes)-1]
		lineAnyMismatch := false
		if !valMatch(oldOut[0], oldOutPatternChar) {
			continue
		}
		for i, patternChar := range inputPatternStr {
			if !(valMatch(inputVals[i][0], patternChar) || edgeMatch(inputEdges[i], patternChar)) {
				lineAnyMismatch = true
				break
			}
		}
		if !lineAnyMismatch { // All match
			switch outVal {
			case '0':
				return Vec{VAL_0_FORMAL}
			case '1':
				return Vec{VAL_1_FORMAL}
			case 'x', 'X':
				return Vec{VAL_X_FORMAL}
			case 'z', 'Z':
				return Vec{VAL_Z_FORMAL}
			case '-':
				return oldOut
			default:
				panic("ERR")
			}
		}
	}
	return Vec{VAL_X_FORMAL}
}

func CreateUDPDef(jsonUDPDef map[string]interface{}, label string) UDPDefIface {
	initVal := jsonUDPDef["init_val"].(string)
	table := SliceString(jsonUDPDef["table_rows"])
	base := UDPDef{
		Name:        jsonUDPDef["name"].(string),
		InputAmount: int(jsonUDPDef["input_amount"].(float64)),
		Table:       table,
	}
	if initVal == "" { // Combinational
		return &CombUDPDef{
			UDPDef: base,
		}
	} else { // Sequential
		return &SeqUDPDef{
			UDPDef:  base,
			InitVal: MakeVecViaStr(jsonUDPDef["init_val"].(string)),
		}
	}
}

func valMatch(valBit Bit, patternChar byte) bool {
	switch valBit {
	case VAL_1_FORMAL:
		switch patternChar {
		case '1', 'b', 'h', '?':
			return true
		}
	case VAL_0_FORMAL:
		switch patternChar {
		case '0', 'b', 'l', '?':
			return true
		}
	case VAL_X_FORMAL, VAL_Z_FORMAL:
		switch patternChar {
		case 'x', 'h', 'l', '?':
			return true
		}
	}
	return false
}

func edgeMatch(edge Edge, patternChar byte) bool {
	switch edge {
	case E_10:
		switch patternChar {
		case '*', 'N', '_', 'f', 'n':
			return true
		}
	case E_1X:
		switch patternChar {
		case '*', 'N', '%', 'M', 'n':
			return true
		}
	case E_X0:
		switch patternChar {
		case '*', 'B', '_', 'F', 'n':
			return true
		}
	case E_X1:
		switch patternChar {
		case '*', 'B', '+', 'R', 'p':
			return true
		}
	case E_01:
		switch patternChar {
		case '*', 'P', '+', 'r', 'p':
			return true
		}
	case E_0X:
		switch patternChar {
		case '*', 'P', '%', 'Q', 'p':
			return true
		}
	case E_NC: // No change
		switch patternChar {
		case '-':
			return true
		}
	}
	return false
}
