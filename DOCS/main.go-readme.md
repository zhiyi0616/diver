# main函数核心代码说明
对于Diver当中最核心的代码莫过于main.go，其包含着Diver的读入JSON网表，初始化网表形成，网表仿真等多个核心操作.如果后续要进行研发对Diver,那么main函数是必须读懂的过程.下面会对主函数当中的代码做一个详细的说明，尤其是其中难于理解的代码进行详细阐述.
下面是Diver当中主函数的执行流程框图，main函数主要执行的事情如下：

- 命令行参数解析
- 文件操作
- 处理JSON网表信息
- 内部网表初始化形成
- 内部网表初始化传播
- 等待给定激励

![](docs-img/main-simulate.png)

## 处理JSON网表信息
当把JSON文件读取到内存当中的时候，Diver分别把 JSON网表当中的 nodes，signal, udp_defention等分类处理但是处理的时候大体思路是一样的.因此我们这里只对udp_defention信息的提取做说明，其它的JSON信息处理大体思路是一致的.
### 处理网表当中的udp信息
主函数当中担负着是读取JSON文件，获取整个网表信息的重要任务。但是当JSON文件信息读入到内存当中时为后期的处理就需要对JSON网表信息进行分类处理。可以看到在主函数中**for循环遍历语句**之前有几个build  map的操作。这里主要就是对网表JSON信息的提取。由于这三个bulid map操作原理大体上都相同，因此本文档只对其中一个进行阐述，只要这个弄清楚其它的就会明了

``` go
// UDP Def map should be built before Nodes
jsonUDPDefMap := jsonRead["udp_definitions"].(map[string]interface{})
fmt.Println("Creating UDP def map...")
for UDPDefLabel := range jsonUDPDefMap {
jsonUDPDef := jsonUDPDefMap[UDPDefLabel].(map[string]interface{})
GUDPDefMap[UDPDefLabel] = CreateUDPDef(jsonUDPDef, UDPDefLabel)
}
```

第1行代码
```go
jsonUDPDefMap := jsonRead["udp_definitions"].(map[string]interface{})
```
主要的目的是把已经读取到内存当中的JSON文件信息提取"udp_definitions"出这一部分.一般一个JSON网表信息大致分为 statements,nodes,signals,threads,scopes,udp_definitions,code_segment几类。而此处就是把JSON文件当中的”udp_definitions”这一JSON小块提出出来，存放在变量jsonUDPDefMap当中。

第3~5行代码
```go
for UDPDefLabel := range jsonUDPDefMap {
jsonUDPDef := jsonUDPDefMap[UDPDefLabel].(map[string]interface{})
GUDPDefMap[UDPDefLabel] = CreateUDPDef(jsonUDPDef, UDPDefLabel)
}
```

我们可以知道在一个"udp_definitions"的json块信息当中含有这几个组成部分.

```json
    "udp_definitions": {
        "UDP_ip_ffsedcr": {
            "name": "ip_ffsedcr",
            "input_amount": 7,
            "table_rows": [
                "?1r110??1",
                "?0r1?0??0",
                ......
            ],
            "init_val": "x"
        },
        UDP_ip_ffsdsr:{
            ...
        }
```

这个for遍历的其实是"udp_definitions"JSON块下面的每一个元素，即上面的每一个“UDP_ip_ffsedcr”，可以把其当成这个"udp_definitions"大JSON块信息下面包含了很多小"UDP_ip_ffsedcr"JSON块。其实这个for的遍历就是把每一个小JSON块"UDP_ip_ffsedcr"里面内含的信息进行分类处理。对小JSON块的信息处理都在CreateUDPDef当中进行。下面是该函数的代码：
```go
func CreateUDPDef(jsonUDPDef map[string]interface{}, label string) UDPDefIface {
	initVal := jsonUDPDef["init_val"].(string)
	table := SliceString(jsonUDPDef["table_rows"])
	base := UDPDef{
		Name:        jsonUDPDef["name"].(string),
		InputAmount: int(jsonUDPDef["input_amount"].(float64)),
		Table:       table,
	}
	if initVal == "" { // Combinational
		return &CombUDPDef{
			UDPDef: base,
		}
	} else { // Sequential
		return &SeqUDPDef{
			UDPDef:  base,
			InitVal: MakeVecViaStr(jsonUDPDef["init_val"].(string)),
		}
	}
}
```
根据上面我们列出"udp_definitions"的JSON信息，很容易知道就是在for循环遍历到每一个 "UDP_ip_ffsedcr"时，把"UDP_ip_ffsedcr"当中的init_val，table_rows等信息提出出来，存放在一个结构体当中然后这个结构体.返回的结构体存储在GUDPDefMap[UDPDefLabel]当中。最终的思路就是把JSON信息提取出来存放在自己定义的内部数据结构GUDPDefMap[UDPDefLabel]当中。
至此就是bulid udp def map的全过程，下面对bulid logic node map, bulid signal map原理大致如此.