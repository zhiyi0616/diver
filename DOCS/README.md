# Docs文件夹说明
由于后期Diver作用是对标iVerilog当中的VVP仿真器，因此代码会涉及很多相关知识。Diver源码阅读起来会稍微的难懂，为此Docs文件夹存放的是有关Diver的所有书面文档以供各个开发者进行学习研究

## 文件目录
- main.go-readme.md  主要说明Diver当中main.go源码文件的核心代码
- support-nodes.md  目前Diver支持的最新节点情况





# 编译运行
在配置好golang的运行环境情况下，首先在命令行运行编译命令：
```shell
go build
```
之后我们会看到在源码目录下会生成Diver.exe可执行文件，这个可执行文件便是我们自主研发的网表仿真器Diver。在完成上述命令之后，就可以进行网表仿真。由于Diver的网表输入文件是JSON格式，因此我们需要找到相应需要仿真的JSON文件。具体的命令行参数可以参考Diver源码当中main.go文件，该文件当中代码详细说明了如何使用
下面给出参考运行命令：
```shell
 ./diver.exe -file-name counter_16.json
```
