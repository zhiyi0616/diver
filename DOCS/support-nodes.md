| 已测结点名字  | 编号 |
| ------------- | ---- |
| N_VAR         | 0    |
| N_FUNC_AND    | 27   |
| N_FUNC_OR     | 28   |
| N_FUNC_NOR    | 30   |
| N_FUNC_XOR    | 32   |
| N_FUNC_NOT    | 33   |
| N_FUNC_BUF    | 36   |
| N_FUNC_BUFIF0 | 37   |
| N_FUNC_BUFIF1 | 38   |
| N_FUNC_BUFT   | 39   |
| N_FUNC_BUFZ   | 40   |
| N_FUNC_NMOS   | 44   |
| N_FUNC_PMOS   | 45   |
| N_CONCAT      | 65   |
| N_CONCAT8     | 66   |
| N_DELAY       | 67   |
| N_VPART_SEL   | 85   |
| N_UDP         | 107  |
| N_ARITH_DIV   | 11   |
| N_ARITH_MULT  | 17   |
| N_ARITH_SUM   | 24   |
| N_ARITH_POW   | 19   |
| N_ARITH_SUB   | 22   |
| N_FUNC_NAND   | 29   |
| N_FUNC_XNOR   | 31   |
| N_FUNC_RNMOS  | 47   |
| N_FUNC_RPMOS  | 48   |
| N_FUNC_CMOS   | 43   |
| N_FUNC_RCMOS  | 46   |



| 未测结点名字                | 编号 |
| --------------------------- | ---- |
| N_VAR_COBJ                  | 1    |
| N_VAR_DARRAY                | 2    |
| N_VAR_QUEUE                 | 3    |
| N_VAR_REAL                  | 4    |
| N_VAR_S                     | 5    |
| N_VAR_STR                   | 6    |
| N_VAR_I                     | 7    |
| N_VAR_2S                    | 8    |
| N_VAR_2U                    | 9    |
| N_ABS                       | 10   |
| N_ARITH_DIV_R               | 12   |
| N_ARITH_DIV_S               | 13   |
| N_ARITH_MOD                 | 14   |
| N_ARITH_MOD_R               | 15   |
| N_ARITH_MOD_S               | 16   |
| N_ARITH_MULT_R              | 18   |
| N_ARITH_POW_R               | 20   |
| N_ARITH_POW_S               | 21   |
| N_ARITH_SUB_R               | 23   |
| N_ARITH_SUM_R               | 25   |
| N_ARRAY_PORT                | 26   |
| N_FUNC_NOTIF0               | 34   |
| N_FUNC_NOTIF1               | 35   |
| N_FUNC_MUXR                 | 41   |
| N_FUNC_MUXZ（已写未测）     | 42   |
| N_CMP_EEQ（已写未测）       | 49   |
| N_CMP_EQX                   | 50   |
| N_CMP_EQZ                   | 51   |
| N_CMP_EQ（已写未测）        | 52   |
| N_CMP_EQ_R                  | 53   |
| N_CMP_NEE（已写未测）       | 54   |
| N_CMP_NE（已写未测）        | 55   |
| N_CMP_NE_R                  | 56   |
| N_CMP_GE                    | 57   |
| N_CMP_GE_R                  | 58   |
| N_CMP_GE_S                  | 59   |
| N_CMP_GT                    | 60   |
| N_CMP_GT_R                  | 61   |
| N_CMP_GT_S                  | 62   |
| N_CMP_WEQ                   | 63   |
| N_CMP_WNE                   | 64   |
| N_DFF_N                     | 68   |
| N_DFF_N_ACLR                | 69   |
| N_DFF_N_ASET                | 70   |
| N_DFF_P                     | 71   |
| N_DFF_P_ACLR                | 72   |
| N_DFF_P_ASET                | 73   |
| N_LATCH                     | 74   |
| N_EVENT_ANYEDGE（已写未测） | 75   |
| N_EVENT_POSEDGE（已写未测） | 76   |
| N_EVENT_NEGEDGE（已写未测） | 77   |
| N_EVENT_OR（已写未测)       | 78   |
| N_EXTEND_S                  | 79   |
| N_TCAST_TO_2                | 80   |
| N_TCAST_TO_INT              | 81   |
| N_TCAST_TO_REAL             | 82   |
| N_TCAST_TO_REAL_S           | 83   |
| N_MODPATH                   | 84   |
| N_VPART_DRIVER（已写未测）  | 86   |
| N_VPART_SEL_V               | 87   |
| N_VPART_SEL_V_S             | 88   |
| N_ISLAND_PORT               | 89   |
| N_ISLAND_IMPORT             | 90   |
| N_ISLAND_EXPORT             | 91   |
| N_REDUCE_AND                | 92   |
| N_REDUCE_OR                 | 93   |
| N_REDUCE_XOR                | 94   |
| N_REDUCE_NAND               | 95   |
| N_REDUCE_NOR                | 96   |
| N_REDUCE_XNOR               | 97   |
| N_REPEAT                    | 98   |
| N_STRENGTH_RESOLV           | 99   |
| N_SHIFT_L                   | 100  |
| N_SHIFT_R                   | 101  |
| N_SHIFT_RS                  | 102  |
| N_SUBSTITUTE                | 103  |
| N_UFUNC_REAL                | 104  |
| N_UFUNC_VEC4                | 105  |
| N_UFUNC_E                   | 106  |
| N_SFUNC                     | 108  |
| N_SFUNC_E                   | 109  |

### 说明
- 算数运算符暂只支持无符号运算












