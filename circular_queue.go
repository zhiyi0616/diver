package main

type CircularQueue struct {
	head   int
	tail   int
	slice_ []interface{}
}

func newCircularQueue(maxLen int) *CircularQueue {
	return &CircularQueue{
		head:   0,
		tail:   0,
		slice_: make([]interface{}, maxLen+1),
	}
}

// Add-in
func (self *CircularQueue) Offer(val interface{}) (ok bool) {
	newTail := self.tail + 1
	if newTail >= len(self.slice_) {
		newTail = 0
	}
	if self.head == newTail { // full
		return false
	} else {
		self.slice_[self.tail] = val
		self.tail = newTail
		return true
	}

}

// Poll out
func (self *CircularQueue) Poll() (val interface{}, ok bool) {
	if self.head == self.tail { // Empty
		return nil, false
	} else {
		res := self.slice_[self.head]
		self.head++
		if self.head >= len(self.slice_) {
			self.head = 0
		}
		return res, true
	}
}

// The capacity
func (self *CircularQueue) GetCap() int {
	return len(self.slice_) - 1
}

func (self *CircularQueue) GetCurrentLen() int {
	if self.tail >= self.head {
		return self.tail - self.head
	} else {
		return self.tail + len(self.slice_) - self.head
	}
}

// The next to poll out. return nil if empty
func (self *CircularQueue) Peek() interface{} {
	if self.head == self.tail { // Empty
		return nil
	} else {
		return self.slice_[self.head]
	}
}
