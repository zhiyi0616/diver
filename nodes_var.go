/*\
*!*  # PROJECT/PROGRAM
*!*  This project is a netlist propagation simulator
*!*  Written by Yusang Luo (luoyusang2007@hotmail.com)
*!*
*!*  # FILE
*!*  This file defines the type of variable nodes
*!*
*!*  Jan 2021
\*/

package main

// A var is a node without any input connection.
// However, it has one input value.
type VarNode struct {
	NetlistNode
	Name string
}

func (self *VarNode) Eval() Vec {
	return self.InputVals[0]
}
