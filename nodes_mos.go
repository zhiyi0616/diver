package main

type MosNode struct {
	NetlistNode
}

const (
	supply uint8 = 7
	strong uint8 = 6
	pull   uint8 = 5
	large  uint8 = 4
	weak   uint8 = 3
	medium uint8 = 2
	small  uint8 = 1
	highz  uint8 = 0
)

// Database:0, control: 1
func (self *MosNode) Eval() Vec {
	selfLen := len(self.InputVals[0])
	if len(self.InputVals[0]) != len(self.InputVals[1]) {
		panic("length not equal !!")
	}
	res := make(Vec, selfLen)
	for i := 0; i < selfLen; i++ {
		switch self.FuncType {
		case N_FUNC_PMOS, N_FUNC_NMOS:
			res[i] = self.InputVals[0][i].BitLogicNPmos(self.InputVals[1][i], self.FuncType)
		case N_FUNC_RPMOS, N_FUNC_RNMOS:
			res[i] = self.InputVals[0][i].BitLogicrNPmos(self.InputVals[1][i], self.FuncType)
		case N_FUNC_CMOS:
			res[i] = self.InputVals[0][i].BitLogicCmos(self.InputVals[1][i], self.InputVals[2][i])
		case N_FUNC_RCMOS:
			res[i] = self.InputVals[0][i].BitLogicRCmos(self.InputVals[1][i], self.InputVals[2][i])
		default:
			panic("err")
		}
	}
	return res
}

// 只要supply7信号经过 nonresistive 强度都会减为strong
func (self Bit) nonresistive() Bit {
	Ldirection, Lstrength := self.PickLdrive()
	Rdirection, _ := self.PickRdrive()
	if supply == Lstrength {
		Lstrength = Lstrength - 1
		return Bit(((Ldirection<<3 | Lstrength) << 4) | ((Rdirection << 3) | Lstrength))
	}
	return self
}

// 处理带有强度信息的时候输出L,H
func (self Bit) ToSingalLH() Bit {
	_, LStrength := self.PickLdrive()
	switch self.ToFormal() {
	case VAL_0_FORMAL:
		return Bit(LStrength | 0x08)
	case VAL_1_FORMAL:
		return Bit(LStrength<<4 | 0x08)
	default:
		panic("H or L input signal can not be x or z")
	}

}

// 实现了Nmos与Pmos的功能
func (self Bit) BitLogicNPmos(Control Bit, nodetype FuncType) Bit {
	inputSignal := self.nonresistive()
	inputCategory := self.ToFormal()
	controlCategory := Control.ToFormal()
	switch controlCategory {
	case VAL_0_FORMAL:
		if nodetype == N_FUNC_PMOS || nodetype == N_FUNC_RPMOS {
			return inputSignal
		} else {
			return VAL_Z_FORMAL
		}
	case VAL_1_FORMAL:
		if nodetype == N_FUNC_PMOS || nodetype == N_FUNC_RPMOS {
			return VAL_Z_FORMAL
		} else {
			return inputSignal
		}
	case VAL_X_FORMAL, VAL_Z_FORMAL:
		switch inputCategory {
		case VAL_0_FORMAL:
			return inputSignal.ToSingalLH()
		case VAL_1_FORMAL:
			return inputSignal.ToSingalLH()
		default:
			return inputSignal
		}
	default:
		panic("err")
	}
}

func reduceStrength(inputSignal Bit) Bit {
	_, LStrength := inputSignal.PickLdrive()
	switch LStrength {
	case supply, pull, large:
		LStrength -= 2
	case strong, weak, medium:
		LStrength -= 1
	case small, highz: //这两个强度无变化
	default:
		panic("err")
	}
	return (inputSignal & 0x88) | Bit((LStrength<<4 | LStrength))
}

func (self Bit) BitLogicrNPmos(control Bit, nodetype FuncType) Bit {
	inputsignal := reduceStrength(self)
	return inputsignal.BitLogicNPmos(control, nodetype)
}

// N_EN port index :1 P_EN port index:2
func (self Bit) BitLogicCmos(Ncontrol Bit, Pcontrol Bit) Bit {
	inputSignal := self.nonresistive() // cmos强度信息属于不变化的情况
	if Ncontrol.ToFormal() == VAL_1_FORMAL || Pcontrol.ToFormal() == VAL_0_FORMAL {
		return inputSignal
	} else if Ncontrol.ToFormal() == VAL_0_FORMAL && Pcontrol.ToFormal() == VAL_1_FORMAL {
		return VAL_Z_FORMAL
	} else {
		switch inputSignal.ToFormal() {
		case VAL_0_FORMAL:
			return inputSignal.ToSingalLH()
		case VAL_1_FORMAL:
			return inputSignal.ToSingalLH()
		default: // 针对网表初始值为X情况
			return inputSignal
		}
	}
}

// N_EN port index :1 P_EN port index:2
func (self Bit) BitLogicRCmos(Ncontrol Bit, Pcontrol Bit) Bit {
	inputsignal := reduceStrength(self)
	return inputsignal.BitLogicCmos(Ncontrol, Pcontrol)
}
