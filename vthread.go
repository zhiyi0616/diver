package main

type VThread struct {
	Pc    int // Program counter
	Stack int // Data stack
}
