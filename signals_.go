package main

type SignalIface interface{}

type Signal struct {
}

type NamedSignal struct {
	Signal
	Name string
}

type SingleValSignal struct {
	Signal
	ValNode NodeIface
}

type ArraySignal struct {
	Signal
	start    int
	end      int
	ValNodes []NodeIface
}

type Net struct {
	NamedSignal
	SingleValSignal
}
type Var struct {
	NamedSignal
	SingleValSignal
}

type NetArray struct {
	NamedSignal
	ArraySignal
}

type VarArray struct {
	NamedSignal
	ArraySignal
}

type Param struct {
	NamedSignal
	Val Vec
}

// To be removed when optimized
type NetArrayCell struct {
	SingleValSignal
	ValNode NodeIface
}

func CreateSignal(jsonSignal map[string]interface{}, label string) SignalIface {
	type_ := jsonSignal["type"].(string)
	switch type_ {
	case "T_PARAM":

	case "T_VAR_ARR":

	case "T_NET_ARR":

	case "T_VAR":

	case "T_NET":

	case "T_NET_ARR_CELL":

	}
	return &Signal{}
}
