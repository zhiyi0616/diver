package main

import "fmt"

type CompareNode struct {
	NetlistNode
}

func (self *CompareNode) Eval() Vec {
	switch self.FuncType {
	case N_CMP_EEQ: // Exactly equal, only returns true or false
		if self.InputVals[0].eeq(self.InputVals[1]) {
			return Vec{VAL_1_FORMAL}
		} else {
			return Vec{VAL_0_FORMAL}
		}
	case N_CMP_NEE: // Not exactly equal, only returns true or false
		if self.InputVals[0].eeq(self.InputVals[1]) {
			return Vec{VAL_0_FORMAL}
		} else {
			return Vec{VAL_1_FORMAL}
		}
	case N_CMP_EQ:
		if len(self.InputVals) != 2 {
			panic("compare should have 2 inputs.")
		}
		if len(self.InputVals[0]) != len(self.InputVals[1]) {
			// FIXME: if compare operands can have different length?
			fmt.Println("CMP Node", self.GetLabelStr(), "cmp1:", self.InputVals[0], "cmp2:", self.InputVals[1])
			panic("Compare length not equal...")
		}
		if self.InputVals[0].mustEq(self.InputVals[1]) {
			return Vec{VAL_1_FORMAL}
		} else if self.InputVals[0].mustNe(self.InputVals[1]) {
			return Vec{VAL_0_FORMAL}
		}
		return Vec{VAL_X_FORMAL}
	default:
		panic("Unknown compare func.")
	}

}
