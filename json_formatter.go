package main

// JSON pattern
type JsonData struct {
	Nodes map[string]struct {
		Inputs []struct {
			VaType string `json:"val_type"`
			ValStr string `json:"val_str"`
		} `json:"inputs"`
		Outputs []string    `json:"outputs"`
		Width   int         `json:"width"`
		Scope   string      `json:"scope"`
		Params  interface{} `json:"params"`
	} `json:"nodes"`
	Scopes map[string]struct {
		Type          string            `json:"type"`
		InstName      string            `json:"inst_name"`
		DefName       string            `json:"def_name"`
		Parent        string            `json:"parent"`
		Children      map[string]string `json:"children"`
		AncestorChain []string          `json:"ancestor_chain"`
		Signals       map[string]string
		Ports         map[string]struct {
			Name      string `json:"name"`
			Direction string `json:"direction"`
		} `json:"ports"`
	} `json:"scopes"`
	Threads map[string]struct {
		Start int `json:"start"`
		End   int `json:"end"`
	} `json:"threads"`
	ProcCodeSegment []struct {
		Scope    string   `json:"scope"`
		Opcode   string   `json:"opcode"`
		Operands []string `json:"operands"`
	} `json:"code_segment"`
}
