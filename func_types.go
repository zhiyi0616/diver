package main

// Enum FuncType
type FuncType uint8

const (
	N_VAR             FuncType = 0   // Supported
	N_VAR_COBJ        FuncType = 1   // Not supported
	N_VAR_DARRAY      FuncType = 2   //
	N_VAR_QUEUE       FuncType = 3   //
	N_VAR_REAL        FuncType = 4   //
	N_VAR_S           FuncType = 5   //
	N_VAR_STR         FuncType = 6   //
	N_VAR_I           FuncType = 7   //
	N_VAR_2S          FuncType = 8   //
	N_VAR_2U          FuncType = 9   //
	N_ABS             FuncType = 10  //
	N_ARITH_DIV       FuncType = 11  //
	N_ARITH_DIV_R     FuncType = 12  //
	N_ARITH_DIV_S     FuncType = 13  //
	N_ARITH_MOD       FuncType = 14  //
	N_ARITH_MOD_R     FuncType = 15  //
	N_ARITH_MOD_S     FuncType = 16  //
	N_ARITH_MULT      FuncType = 17  // Supported
	N_ARITH_MULT_R    FuncType = 18  //
	N_ARITH_POW       FuncType = 19  // Supported
	N_ARITH_POW_R     FuncType = 20  //
	N_ARITH_POW_S     FuncType = 21  //
	N_ARITH_SUB       FuncType = 22  // Supported
	N_ARITH_SUB_R     FuncType = 23  //
	N_ARITH_SUM       FuncType = 24  // Supported
	N_ARITH_SUM_R     FuncType = 25  //
	N_ARRAY_PORT      FuncType = 26  //
	N_FUNC_AND        FuncType = 27  // Supported
	N_FUNC_OR         FuncType = 28  // Supported
	N_FUNC_NAND       FuncType = 29  // Supported
	N_FUNC_NOR        FuncType = 30  // Supported
	N_FUNC_XNOR       FuncType = 31  // Supported
	N_FUNC_XOR        FuncType = 32  // Supported
	N_FUNC_NOT        FuncType = 33  // Supported
	N_FUNC_NOTIF0     FuncType = 34  //
	N_FUNC_NOTIF1     FuncType = 35  //
	N_FUNC_BUF        FuncType = 36  // Supported
	N_FUNC_BUFIF0     FuncType = 37  //
	N_FUNC_BUFIF1     FuncType = 38  //
	N_FUNC_BUFT       FuncType = 39  // Supported
	N_FUNC_BUFZ       FuncType = 40  // Supported
	N_FUNC_MUXR       FuncType = 41  //
	N_FUNC_MUXZ       FuncType = 42  // Supported*
	N_FUNC_CMOS       FuncType = 43  //
	N_FUNC_NMOS       FuncType = 44  //
	N_FUNC_PMOS       FuncType = 45  //
	N_FUNC_RCMOS      FuncType = 46  //
	N_FUNC_RNMOS      FuncType = 47  //
	N_FUNC_RPMOS      FuncType = 48  //
	N_CMP_EEQ         FuncType = 49  // Supported
	N_CMP_EQX         FuncType = 50  //
	N_CMP_EQZ         FuncType = 51  //
	N_CMP_EQ          FuncType = 52  // Supported
	N_CMP_EQ_R        FuncType = 53  //
	N_CMP_NEE         FuncType = 54  // Supported
	N_CMP_NE          FuncType = 55  // Supported
	N_CMP_NE_R        FuncType = 56  //
	N_CMP_GE          FuncType = 57  // ...
	N_CMP_GE_R        FuncType = 58  //
	N_CMP_GE_S        FuncType = 59  //
	N_CMP_GT          FuncType = 60  // ...
	N_CMP_GT_R        FuncType = 61  //
	N_CMP_GT_S        FuncType = 62  //
	N_CMP_WEQ         FuncType = 63  //
	N_CMP_WNE         FuncType = 64  //
	N_CONCAT          FuncType = 65  //
	N_CONCAT8         FuncType = 66  // Supported
	N_DELAY           FuncType = 67  //
	N_DFF_N           FuncType = 68  //
	N_DFF_N_ACLR      FuncType = 69  //
	N_DFF_N_ASET      FuncType = 70  //
	N_DFF_P           FuncType = 71  //
	N_DFF_P_ACLR      FuncType = 72  //
	N_DFF_P_ASET      FuncType = 73  //
	N_LATCH           FuncType = 74  //
	N_EVENT_ANYEDGE   FuncType = 75  //
	N_EVENT_POSEDGE   FuncType = 76  //
	N_EVENT_NEGEDGE   FuncType = 77  //
	N_EVENT_OR        FuncType = 78  //
	N_EXTEND_S        FuncType = 79  //
	N_TCAST_TO_2      FuncType = 80  //
	N_TCAST_TO_INT    FuncType = 81  //
	N_TCAST_TO_REAL   FuncType = 82  //
	N_TCAST_TO_REAL_S FuncType = 83  //
	N_MODPATH         FuncType = 84  //
	N_VPART_SEL       FuncType = 85  //
	N_VPART_DRIVER    FuncType = 86  //
	N_VPART_SEL_V     FuncType = 87  //
	N_VPART_SEL_V_S   FuncType = 88  //
	N_ISLAND_PORT     FuncType = 89  //
	N_ISLAND_IMPORT   FuncType = 90  //
	N_ISLAND_EXPORT   FuncType = 91  //
	N_REDUCE_AND      FuncType = 92  //
	N_REDUCE_OR       FuncType = 93  //
	N_REDUCE_XOR      FuncType = 94  //
	N_REDUCE_NAND     FuncType = 95  //
	N_REDUCE_NOR      FuncType = 96  //
	N_REDUCE_XNOR     FuncType = 97  //
	N_REPEAT          FuncType = 98  //
	N_STRENGTH_RESOLV FuncType = 99  //
	N_SHIFT_L         FuncType = 100 //
	N_SHIFT_R         FuncType = 101 //
	N_SHIFT_RS        FuncType = 102 //
	N_SUBSTITUTE      FuncType = 103 //
	N_UFUNC_REAL      FuncType = 104 //
	N_UFUNC_VEC4      FuncType = 105 //
	N_UFUNC_E         FuncType = 106 //
	N_UDP             FuncType = 107 //	  Supported
	N_SFUNC           FuncType = 108 //
	N_SFUNC_E         FuncType = 109 //
)

func MakeFuncType(str string) FuncType {
	switch str {
	case "N_VAR":
		return N_VAR
	case "N_VAR_COBJ":
		return N_VAR_COBJ
	case "N_VAR_DARRAY":
		return N_VAR_DARRAY
	case "N_VAR_QUEUE":
		return N_VAR_QUEUE
	case "N_VAR_REAL":
		return N_VAR_REAL
	case "N_VAR_S":
		return N_VAR_S
	case "N_VAR_STR":
		return N_VAR_STR
	case "N_VAR_I":
		return N_VAR_I
	case "N_VAR_2S":
		return N_VAR_2S
	case "N_VAR_2U":
		return N_VAR_2U
	case "N_ABS":
		return N_ABS
	case "N_ARITH_DIV":
		return N_ARITH_DIV
	case "N_ARITH_DIV_R":
		return N_ARITH_DIV_R
	case "N_ARITH_DIV_S":
		return N_ARITH_DIV_S
	case "N_ARITH_MOD":
		return N_ARITH_MOD
	case "N_ARITH_MOD_R":
		return N_ARITH_MOD_R
	case "N_ARITH_MOD_S":
		return N_ARITH_MOD_S
	case "N_ARITH_MULT":
		return N_ARITH_MULT
	case "N_ARITH_MULT_R":
		return N_ARITH_MULT_R
	case "N_ARITH_POW":
		return N_ARITH_POW
	case "N_ARITH_POW_R":
		return N_ARITH_POW_R
	case "N_ARITH_POW_S":
		return N_ARITH_POW_S
	case "N_ARITH_SUB":
		return N_ARITH_SUB
	case "N_ARITH_SUB_R":
		return N_ARITH_SUB_R
	case "N_ARITH_SUM":
		return N_ARITH_SUM
	case "N_ARITH_SUM_R":
		return N_ARITH_SUM_R
	case "N_ARRAY_PORT":
		return N_ARRAY_PORT
	case "N_FUNC_AND":
		return N_FUNC_AND
	case "N_FUNC_OR":
		return N_FUNC_OR
	case "N_FUNC_NAND":
		return N_FUNC_NAND
	case "N_FUNC_NOR":
		return N_FUNC_NOR
	case "N_FUNC_XNOR":
		return N_FUNC_XNOR
	case "N_FUNC_XOR":
		return N_FUNC_XOR
	case "N_FUNC_NOT":
		return N_FUNC_NOT
	case "N_FUNC_NOTIF0":
		return N_FUNC_NOTIF0
	case "N_FUNC_NOTIF1":
		return N_FUNC_NOTIF1
	case "N_FUNC_BUF":
		return N_FUNC_BUF
	case "N_FUNC_BUFIF0":
		return N_FUNC_BUFIF0
	case "N_FUNC_BUFIF1":
		return N_FUNC_BUFIF1
	case "N_FUNC_BUFT":
		return N_FUNC_BUFT
	case "N_FUNC_BUFZ":
		return N_FUNC_BUFZ
	case "N_FUNC_MUXR":
		return N_FUNC_MUXR
	case "N_FUNC_MUXZ":
		return N_FUNC_MUXZ
	case "N_FUNC_CMOS":
		return N_FUNC_CMOS
	case "N_FUNC_NMOS":
		return N_FUNC_NMOS
	case "N_FUNC_PMOS":
		return N_FUNC_PMOS
	case "N_FUNC_RCMOS":
		return N_FUNC_RCMOS
	case "N_FUNC_RNMOS":
		return N_FUNC_RNMOS
	case "N_FUNC_RPMOS":
		return N_FUNC_RPMOS
	case "N_CMP_EEQ":
		return N_CMP_EEQ
	case "N_CMP_EQX":
		return N_CMP_EQX
	case "N_CMP_EQZ":
		return N_CMP_EQZ
	case "N_CMP_EQ":
		return N_CMP_EQ
	case "N_CMP_EQ_R":
		return N_CMP_EQ_R
	case "N_CMP_NEE":
		return N_CMP_NEE
	case "N_CMP_NE":
		return N_CMP_NE
	case "N_CMP_NE_R":
		return N_CMP_NE_R
	case "N_CMP_GE":
		return N_CMP_GE
	case "N_CMP_GE_R":
		return N_CMP_GE_R
	case "N_CMP_GE_S":
		return N_CMP_GE_S
	case "N_CMP_GT":
		return N_CMP_GT
	case "N_CMP_GT_R":
		return N_CMP_GT_R
	case "N_CMP_GT_S":
		return N_CMP_GT_S
	case "N_CMP_WEQ":
		return N_CMP_WEQ
	case "N_CMP_WNE":
		return N_CMP_WNE
	case "N_CONCAT":
		return N_CONCAT
	case "N_CONCAT8":
		return N_CONCAT8
	case "N_DELAY":
		return N_DELAY
	case "N_DFF_N":
		return N_DFF_N
	case "N_DFF_N_ACLR":
		return N_DFF_N_ACLR
	case "N_DFF_N_ASET":
		return N_DFF_N_ASET
	case "N_DFF_P":
		return N_DFF_P
	case "N_DFF_P_ACLR":
		return N_DFF_P_ACLR
	case "N_DFF_P_ASET":
		return N_DFF_P_ASET
	case "N_LATCH":
		return N_LATCH
	case "N_EVENT_ANYEDGE":
		return N_EVENT_ANYEDGE
	case "N_EVENT_POSEDGE":
		return N_EVENT_POSEDGE
	case "N_EVENT_NEGEDGE":
		return N_EVENT_NEGEDGE
	case "N_EVENT_OR":
		return N_EVENT_OR
	case "N_EXTEND_S":
		return N_EXTEND_S
	case "N_TCAST_TO_2":
		return N_TCAST_TO_2
	case "N_TCAST_TO_INT":
		return N_TCAST_TO_INT
	case "N_TCAST_TO_REAL":
		return N_TCAST_TO_REAL
	case "N_TCAST_TO_REAL_S":
		return N_TCAST_TO_REAL_S
	case "N_MODPATH":
		return N_MODPATH
	case "N_VPART_SEL":
		return N_VPART_SEL
	case "N_VPART_DRIVER":
		return N_VPART_DRIVER
	case "N_VPART_SEL_V":
		return N_VPART_SEL_V
	case "N_VPART_SEL_V_S":
		return N_VPART_SEL_V_S
	case "N_ISLAND_PORT":
		return N_ISLAND_PORT
	case "N_ISLAND_IMPORT":
		return N_ISLAND_IMPORT
	case "N_ISLAND_EXPORT":
		return N_ISLAND_EXPORT
	case "N_REDUCE_AND":
		return N_REDUCE_AND
	case "N_REDUCE_OR":
		return N_REDUCE_OR
	case "N_REDUCE_XOR":
		return N_REDUCE_XOR
	case "N_REDUCE_NAND":
		return N_REDUCE_NAND
	case "N_REDUCE_NOR":
		return N_REDUCE_NOR
	case "N_REDUCE_XNOR":
		return N_REDUCE_XNOR
	case "N_REPEAT":
		return N_REPEAT
	case "N_STRENGTH_RESOLV":
		return N_STRENGTH_RESOLV
	case "N_SHIFT_L":
		return N_SHIFT_L
	case "N_SHIFT_R":
		return N_SHIFT_R
	case "N_SHIFT_RS":
		return N_SHIFT_RS
	case "N_SUBSTITUTE":
		return N_SUBSTITUTE
	case "N_UFUNC_REAL":
		return N_UFUNC_REAL
	case "N_UFUNC_VEC4":
		return N_UFUNC_VEC4
	case "N_UFUNC_E":
		return N_UFUNC_E
	case "N_UDP":
		return N_UDP
	case "N_SFUNC":
		return N_SFUNC
	case "N_SFUNC_E":
		return N_SFUNC_E
	default:
		println("Unsupported FuncType:", str)
		panic("Unknown FuncType")
	}
}
