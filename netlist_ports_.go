package main

type NetlistExport struct {
	NetlistNode
	TargetNetConn int
}

type NetlistDump struct {
	NetlistNode
	TargetFile int
}
