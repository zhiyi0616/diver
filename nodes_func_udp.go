package main

type FuncCombUDPNode struct {
	NetlistNode
	DefBodyP *CombUDPDef
}

// Need test
func (self *FuncCombUDPNode) Eval() Vec {
	return self.DefBodyP.CombEval(self.InputVals)
}

type FuncSeqUDPNode struct {
	NetlistNode            // Currently it is not schedulable, which means no more than 1 edges will be found during eval
	DefBodyP    *SeqUDPDef // The definition body pointer
	InputEdges  []Edge     // Store edges when receive. Edges are set "-" during init.
	OutVal      Vec        // Out value should be stored
}

func (self *FuncSeqUDPNode) Receive(portIndex int, newVal Vec) (needSchedule bool, needSend bool) {
	oldValBit := self.InputVals[portIndex][0]
	newValBit := newVal[0]
	if newValBit == oldValBit { // If no change
		return false, false // No change
	}

	// Set Edge Types
	switch oldValBit {
	case VAL_1_FORMAL:
		switch newValBit {
		case VAL_0_FORMAL:
			self.InputEdges[portIndex] = E_10
		case VAL_X_FORMAL, VAL_Z_FORMAL:
			self.InputEdges[portIndex] = E_1X
		}
	case VAL_0_FORMAL:
		switch newValBit {
		case VAL_1_FORMAL:
			self.InputEdges[portIndex] = E_01
		case VAL_X_FORMAL, VAL_Z_FORMAL:
			self.InputEdges[portIndex] = E_0X
		}
	case VAL_X_FORMAL, VAL_Z_FORMAL:
		switch newValBit {
		case VAL_0_FORMAL:
			self.InputEdges[portIndex] = E_X0
		case VAL_1_FORMAL:
			self.InputEdges[portIndex] = E_X1
		}
	}
	self.SetInputVal(portIndex, newVal)
	return false, true
}
func (self *FuncSeqUDPNode) Eval() Vec {
	//fmt.Println("in-seq-udp-node", self.InputVals, self.InputEdges, self.OutVal)

	outVal := self.DefBodyP.SeqEval(self.InputVals, self.InputEdges, self.OutVal)
	// Clear edges
	for i := range self.InputEdges {
		self.InputEdges[i] = E_NC
	}
	self.OutVal = outVal // Set old val
	return outVal
}
