package main

import (
	"fmt"
)

/*
  Strength-Bits:
  	[orientBit][stg1-Bit2][stg1-Bit1][stg1-Bit0] [orientBit][stg0-Bit2][stg0-Bit1][stg0-Bit0]

  VAL_1_FORMAL:        11101110b
  VAL_0_FORMAL:        01100110b

  VAL_X_FORMAL: 	   01101110b
  VAL_Z_FORMAL: 	   00001000b

  VAL_H	      :        0???1000
  VAL_L		  :        00001???

  orientBit: Representative the position of strength


*/
type Bit uint8

const (
	VAL_1_FORMAL Bit = 0xee //  11101110b   DEC: 238
	VAL_0_FORMAL Bit = 0x66 //  01100110b	DEC: 102
	VAL_X_FORMAL Bit = 0x6e //  01101110b	DEC: 110
	VAL_Z_FORMAL Bit = 0x08 //  00001000b	DEC: 8

)

func (self Bit) Is4StatesFormal() bool {
	switch self {
	case VAL_0_FORMAL, VAL_1_FORMAL, VAL_X_FORMAL, VAL_Z_FORMAL:
		return true
	}
	return false
}

// 取出8个bit中高四位的方向为和长度信息
func (self Bit) PickLdrive() (uint8, uint8) {
	pickBits := uint8((self & 0xf0) >> 4)
	driveStrength := pickBits & 0x07
	driveDirection := pickBits >> 3
	return driveDirection, driveStrength
}

// 对低四位操作，作用同上
func (self Bit) PickRdrive() (uint8, uint8) {
	pickBits := uint8(self & 0x0f)
	driveStrength := pickBits & 0x07
	driveDirection := (pickBits >> 3)
	return driveDirection, driveStrength
}

// 判断是否为L,H信号
func (self Bit) AssertStrength() {
	_, LStrength := self.PickLdrive()
	_, RStrength := self.PickRdrive()
	if LStrength != RStrength {
		panic("strength not ewqual")
	}
}

// 把输入信号按照对应的FORMAL输出
func (self Bit) ToFormal() Bit {
	LDirection, LStrength := self.PickLdrive()
	RDirection, RStrength := self.PickRdrive()
	if LDirection == RDirection {
		switch LDirection {
		case 1:
			return VAL_1_FORMAL
		case 0:
			return VAL_0_FORMAL
		default:
			panic("err")
		}
	} else {
		//如果 L/RStrength相加为0则L/Rstrength都为0
		if 0 == LStrength+RStrength {
			return VAL_Z_FORMAL
		} else {
			return VAL_X_FORMAL
		}
	}
}

func (self Bit) Invert() Bit {
	switch self {
	case VAL_0_FORMAL:
		return VAL_1_FORMAL
	case VAL_1_FORMAL:
		return VAL_0_FORMAL
	case VAL_Z_FORMAL:
		return VAL_X_FORMAL
	case VAL_X_FORMAL:
		return VAL_X_FORMAL
	default:
		panic("Only 2or4-state Bits can do inverting.")
	}
}

// Non-method version of Invert
func BitLogicNot(bit Bit) Bit {
	return bit.Invert()
}

// Remove strength singal z, transform z to x. it contained three types of signal
func (self Bit) To01x() Bit {
	if VAL_Z_FORMAL == self.ToFormal() {
		return VAL_X_FORMAL
	} else {
		return self.ToFormal()
	}
}

// AND gate
func BitLogicAnd(inputSingal1 Bit, inputSignal2 Bit) Bit {
	inputSingal1 = inputSingal1.ToFormal()
	inputSignal2 = inputSignal2.ToFormal()

	switch inputSingal1 {
	case VAL_0_FORMAL:
		return VAL_0_FORMAL
	case VAL_1_FORMAL:
		{
			switch inputSignal2 {
			case VAL_0_FORMAL:
				return VAL_0_FORMAL
			case VAL_1_FORMAL:
				return VAL_1_FORMAL
			case VAL_X_FORMAL, VAL_Z_FORMAL:
				return VAL_X_FORMAL
			default:
				panic("AND gate err")
			}
		}
	case VAL_X_FORMAL:
		{
			if VAL_0_FORMAL == inputSignal2 {
				return VAL_0_FORMAL
			} else {
				return VAL_X_FORMAL
			}
		}
	case VAL_Z_FORMAL:
		{
			if VAL_0_FORMAL == inputSignal2 {
				return VAL_0_FORMAL
			} else {
				return VAL_X_FORMAL
			}
		}
	default:
		panic("AND gate err!!")
	}
}

// OR gate
func BitLogicOr(inputSingal1 Bit, inputSignal2 Bit) Bit {
	inputSingal1 = inputSingal1.ToFormal()
	inputSignal2 = inputSignal2.ToFormal()

	switch inputSingal1 {
	case VAL_0_FORMAL:
		{
			switch inputSignal2 {
			case VAL_0_FORMAL:
				return VAL_0_FORMAL
			case VAL_1_FORMAL:
				return VAL_1_FORMAL
			case VAL_X_FORMAL, VAL_Z_FORMAL:
				return VAL_X_FORMAL
			default:
				panic("OR gate err !!")
			}
		}
	case VAL_1_FORMAL:
		return VAL_1_FORMAL
	case VAL_X_FORMAL, VAL_Z_FORMAL:
		{
			if VAL_1_FORMAL == inputSignal2 {
				return VAL_1_FORMAL
			} else {
				return VAL_X_FORMAL
			}
		}
	default:
		panic("OR gate err !!")
	}
}

// Inputs should be formal 01xz
// Output is formal 01x
func BitLogicXor(bit1 Bit, bit2 Bit) Bit {
	if bit1 == VAL_X_FORMAL || bit2 == VAL_X_FORMAL || bit1 == VAL_Z_FORMAL || bit2 == VAL_Z_FORMAL {
		return VAL_X_FORMAL
	} else if (bit1 == VAL_1_FORMAL && bit2 == VAL_0_FORMAL) || (bit1 == VAL_0_FORMAL && bit2 == VAL_1_FORMAL) {
		return VAL_1_FORMAL
	} else if (bit1 == VAL_1_FORMAL && bit2 == VAL_1_FORMAL) || (bit1 == VAL_0_FORMAL && bit2 == VAL_0_FORMAL) {
		return VAL_0_FORMAL
	} else {

		panic("...")
	}
}

////// Vector ////
/*
  Vec is slice of Bits
*/
type Vec []Bit

//// Constructors //
// Make 2-state Vec
func MakeVecViaU64(num uint64, width int) Vec {
	selfLen := width
	res := make(Vec, selfLen)
	res.setFromUnsigned64(num)
	return res
}

func MakeVecViaCharRepeating(a rune, repeat_time int) Vec {
	selfLen := repeat_time
	res := make(Vec, selfLen)
	var bit_val Bit
	switch a {
	case '0':
		bit_val = VAL_0_FORMAL
	case '1':
		bit_val = VAL_1_FORMAL
	case 'x', 'X':
		bit_val = VAL_X_FORMAL
	case 'z', 'Z':
		bit_val = VAL_Z_FORMAL
	default:
		panic("Unknown character in string while making vec.")
	}

	for i := 0; i < selfLen; i++ {
		res[i] = bit_val // 此处用到了 Verilog 当中的 vector【向量】，【标量】规则
	}
	return res
}

// Make 2 or 4 state Vec
func MakeVecViaStr(str string) Vec {
	selfLen := len(str)
	res := make(Vec, selfLen)
	for i, char := range str {
		switch char {
		case '0':
			res[i] = VAL_0_FORMAL
		case '1':
			res[i] = VAL_1_FORMAL
		case 'x', 'X':
			res[i] = VAL_X_FORMAL
		case 'z', 'Z':
			res[i] = VAL_Z_FORMAL
		default:
			panic("Unknown character in string while making vec.")
		}
	}
	return res
}

func (self Vec) Width() int {
	return len(self)
}

//// Exporting Methods //
// bit-wise
// Warning: will not check if is 2-state
func (self Vec) toI64() int64 {
	var res int64 = 0
	selfLen := len(self)

	if selfLen > 64 {
		panic("Vector too long, not supported.")
	}
	for i := selfLen - 1; i >= 0; i-- {
		res += int64((self[i] & 0x1) << (selfLen - i - 1))
	}
	return res
}

// // Warning: will not check if is 2-state
// func (self Vec) toU64() uint64 {
// 	var res uint64 = 0
// 	selfLen := len(self)
// 	if selfLen > 64 {
// 		panic("Vector too long, not supported.")
// 	}
// 	for i := selfLen - 1; i >= 0; i-- {
// 		res += uint64((self[i] & 0x1) << (selfLen - i - 1))
// 	}
// 	return res
// }

func (self Vec) toU64() uint64 {
	var result uint64 = 0
	var temp_num uint64 = 1
	if len(self) > 32 {
		panic("input value length > 32, unsupport!!")
	}
	for idx := len(self) - 1; idx >= 0; idx-- {
		switch self[idx].ToFormal() {
		case VAL_0_FORMAL:
			break
		case VAL_1_FORMAL:
			result = result | temp_num
		case VAL_X_FORMAL, VAL_Z_FORMAL:
			break
		default:
			panic("err")
		}
		temp_num = temp_num << 1
	}
	return result
}

func (self Vec) toString() string {
	selfLen := len(self)
	var res_r = make([]rune, selfLen)
	for i := 0; i < selfLen; i++ {
		switch self[i].ToFormal() {
		case VAL_0_FORMAL:
			res_r[i] = '0'
		case VAL_1_FORMAL:
			res_r[i] = '1'
		case VAL_Z_FORMAL:
			res_r[i] = 'z'
		case VAL_X_FORMAL:
			res_r[i] = 'x'
		default:
			res_r[i] = '?'
		}
	}
	return string(res_r)
}

func (self Vec) setFromUnsigned64(num uint64) {
	selfLen := len(self)
	if num >= (uint64(1)<<selfLen) && selfLen != 64 {
		println("NUM:", num)
		panic("Num too large.")
	}
	for i := 0; i < selfLen; i++ {
		if num&0x01 == 1 {
			self[len(self)-i-1] = VAL_1_FORMAL
		} else {
			self[len(self)-i-1] = VAL_0_FORMAL
		}
		num = num >> 1
	}
}

// Set via copying other vec, only copy value, in-situ
func (self Vec) setViaCopy(src Vec) {
	selfLen := len(self)
	if selfLen != len(src) {
		panic("Length not equal!")
	}
	copy(self, src)
}

//// Non-in-situ Operations //

// combine / partial set
// Length is max(len(self),len(other) + offset)
func (self Vec) combine(other Vec, offset int) Vec {
	selfLen := len(self)
	targetLen := len(other) + offset
	if selfLen > targetLen {
		targetLen = selfLen
	}
	res := make(Vec, targetLen)
	copy(res, self)
	for i := range other {
		res[offset+i] = other[i]
	}
	return res
}

// for 2or4-state only, return 4-state
func (self Vec) changeZ2X() Vec {
	selfLen := len(self)
	res := make(Vec, selfLen)
	for i := 0; i < selfLen; i++ {
		switch self[i] {
		case VAL_0_FORMAL:
			res[i] = VAL_0_FORMAL
		case VAL_1_FORMAL:
			res[i] = VAL_1_FORMAL
		case VAL_Z_FORMAL, VAL_X_FORMAL:
			res[i] = VAL_X_FORMAL
		default:
			panic("Only 2or4-state Bits can changeZ2X.")
		}
	}
	return res
}

// for 2-state only
func (self Vec) bitwiseInvert() Vec {
	selfLen := len(self)
	res := make(Vec, selfLen)
	for i := 0; i < selfLen; i++ {
		switch self[i] {
		case VAL_0_FORMAL:
			res[i] = VAL_1_FORMAL
		case VAL_1_FORMAL:
			res[i] = VAL_0_FORMAL
		default:
			panic("Only 2-state vec can be inverted.")
		}
	}
	return res
}

// for 4-state
func (self Vec) bitwiseInvertX() Vec {
	selfLen := len(self)
	res := make(Vec, selfLen)
	for i := 0; i < selfLen; i++ {
		res[i] = self[i].Invert()
	}
	return res
}

func (self Vec) bitwiseOr(other Vec) Vec {
	selfLen := len(self)
	res := make(Vec, selfLen)
	if len(other) != selfLen {
		panic("Length not equal...")
	}
	for i := 0; i < selfLen; i++ {
		res[i] = BitLogicOr(self[i], other[i])
	}

	return res
}

func (self Vec) bitwiseAnd(other Vec) Vec {
	selfLen := len(self)
	res := make(Vec, selfLen)
	if len(other) != selfLen {
		panic("Length not equal...")
	}
	for i := 0; i < selfLen; i++ {
		res[i] = BitLogicAnd(self[i], other[i])
	}
	return res
}

func (self Vec) bitwiseXor(other Vec) Vec {
	selfLen := len(self)
	res := make(Vec, selfLen)
	if len(other) != selfLen {
		fmt.Println(self, other)
		panic("Length not equal...")
	}
	for i := 0; i < selfLen; i++ {
		res[i] = BitLogicXor(self[i], other[i])
	}
	return res
}

func (self Vec) bitwiseXnor(other Vec) Vec {
	selfLen := len(self)
	res := make(Vec, selfLen)
	if len(other) != selfLen {
		panic("Length not equal...")
	}
	for i := 0; i < selfLen; i++ {
		res[i] = BitLogicXor(self[i], other[i]).Invert()
	}
	return res
}

func (self Vec) bitwiseNand(other Vec) Vec {
	selfLen := len(self)
	res := make(Vec, selfLen)
	if len(other) != selfLen {
		panic("Length not equal...")
	}
	for i := 0; i < selfLen; i++ {
		res[i] = BitLogicAnd(self[i], other[i]).Invert()
	}
	return res
}

func (self Vec) bitwiseNor(other Vec) Vec {
	selfLen := len(self)
	res := make(Vec, selfLen)
	if len(other) != selfLen {
		panic("Length not equal...")
	}
	for i := 0; i < selfLen; i++ {
		res[i] = BitLogicOr(self[i], other[i]).Invert()
	}
	return res
}

//// Comparings //
// Returns true if the 2 Vecs have no chance to be unequal
// If non-1-nor-0 in Vec, return false
// This is more strict than eeq.
func (self Vec) mustEq(other Vec) bool {
	selfLen := len(self)
	if selfLen != len(other) {
		panic("EQ operation have different length...")
	}
	for i := 0; i < selfLen; i++ {
		if other[i] != self[i] {
			return false
		}
		if other[i] != VAL_1_FORMAL && other[i] != VAL_0_FORMAL {
			return false
		}
		if self[i] != VAL_1_FORMAL && self[i] != VAL_0_FORMAL {
			return false
		}
	}
	return true
}

// Returns true if the 2 Vecs have no chance to be equal
// if non-1-nor-0 in Vec, return false
func (self Vec) mustNe(other Vec) bool {
	selfLen := len(self)
	if selfLen != len(other) {
		panic("EQ operation have different length...")
	}
	haveNe := false
	for i := 0; i < selfLen; i++ {
		if other[i] != VAL_1_FORMAL && other[i] != VAL_0_FORMAL {
			return false
		}
		if self[i] != VAL_1_FORMAL && self[i] != VAL_0_FORMAL {
			return false
		}
		if other[i] != self[i] {
			haveNe = true
		}
	}
	return haveNe
}

// Exactly Equal. x=x and z=z, same strength
// 2-state, 4-state or with strength
func (self Vec) eeq(other Vec) bool {
	selfLen := len(self)
	if selfLen != len(other) {
		panic("EEQ operation have different length...")
	}
	for i := 0; i < selfLen; i++ {
		if other[i] != self[i] {
			return false
		}
	}
	return true
}

// Equal if x==z
func (self Vec) eqWhenXeqZ(other Vec) bool {

	return false
}

//// Checking Methods //
// Check if there is any 4-state or 8-state bits
func (self Vec) hasNot01() bool {
	for bit := range self {
		if bit&0xfe != 0 {
			return false
		}
	}
	return true
}

func (self Vec) hasXZ() bool {

	return false
}

////// Array ////
type VecArray []Vec
