package main

import "fmt"

func TestVec() {
	println("////////// TEST VEC //////////")
	vec1 := make(Vec, 8)
	vec2 := make(Vec, 8)
	vec1.setFromUnsigned64(0x02)
	vec2.setFromUnsigned64(0xf2) //242
	//vec3 := vec1.mul(vec2)
	fmt.Println("VEC:", vec1)
	fmt.Println("VEC1 Concat", vec2.combine(vec1, 1))
}
