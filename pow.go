package main

import "fmt"

/*
	实现一个数的整数次方
	pow(x, n)
*/

func Pow(base int, n int) int {
	if base == 0 && n == 0 {
		panic("Can not calc 0**0")
	} else if base == 0 {
		return 0
	} else if n < 0 {
		panic("Pow can not be negative")
	} else {
		return calPow(base, n)
	}
}

func calPow(x int, n int) int {
	if n == 0 {
		return 1
	}
	if n == 1 {
		return x
	}

	// 向右移动一位
	result := calPow(x, n>>1)
	result *= result

	// 如果n是奇数
	if n&1 == 1 {
		result *= x
	}

	return result
}

func testPow() {
	fmt.Println(Pow(3, 4))
	fmt.Println(Pow(6, 2))
	fmt.Println(Pow(2, 6))
	fmt.Println(Pow(16, 1))
}
