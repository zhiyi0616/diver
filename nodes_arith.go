package main

import "fmt"

type FuncArithDoubleInNode struct {
	SchedulableNode
}

func (self *FuncArithDoubleInNode) Eval() Vec {
	inputsLen := len(self.InputVals)
	if inputsLen != 2 {
		fmt.Println("Input amount:", inputsLen)
		panic("Input amount should be 2.")
	}
	switch self.FuncType {
	case N_ARITH_MULT:
		return self.InputVals[0].mul(self.InputVals[1])
	case N_ARITH_DIV:
		return self.InputVals[0].div(self.InputVals[1])
	case N_ARITH_POW:
		return self.InputVals[0].pow(self.InputVals[1])
	case N_ARITH_SUB:
		return self.InputVals[0].sub(self.InputVals[1])
	case N_ARITH_SUM:
		return self.InputVals[0].sum(self.InputVals[1])
	default:
		panic("Unsupported.")
	}
}

// Arith
func (self Vec) sum(other Vec) Vec {
	selfLen := len(self)
	res := make(Vec, selfLen)
	if len(other) != selfLen {
		panic("Length not equal...")
	}
	if self.hasValx() || other.hasValx() {
		return res.arithToX()
	}
	res64 := self.toU64() + other.toU64()
	res.setFromUnsigned64(res64)
	return res
}

func (self Vec) sub(other Vec) Vec {
	selfLen := len(self)
	res := make(Vec, selfLen)
	if len(other) != selfLen {
		panic("Length not equal...")
	}
	if self.hasValx() || other.hasValx() {
		return res.arithToX()
	}
	a := self.toU64()
	b := other.toU64()
	var resU64 uint64
	if a >= b {
		resU64 = a - b
	} else {
		resU64 = a + (1 << selfLen) - b
	}
	res.setFromUnsigned64(resU64)
	return res
}

func (self Vec) pow(other Vec) Vec {
	selfLen := len(self)
	res := make(Vec, selfLen)
	// if len(other) != selfLen {
	// 	panic("Length not equal...")
	// }
	if self.hasValx() || other.hasValx() {
		return res.arithToX()
	}
	resU64 := uint64(Pow(int(self.toU64()), int(other.toU64())))
	res.setFromUnsigned64(resU64)
	return res
}

func (self Vec) mul(other Vec) Vec {
	selfLen := len(self)
	res := make(Vec, selfLen)
	modBase := uint64(1) << selfLen
	if len(other) != selfLen {
		panic("Length not equal...")
	}
	if self.hasValx() || other.hasValx() {
		return res.arithToX()
	}
	resU64 := self.toU64() * other.toU64()
	resU64 = resU64 % modBase // Result could be too long
	res.setFromUnsigned64(resU64)
	return res
}

func (self Vec) div(other Vec) Vec {
	selfLen := len(self)
	res := make(Vec, selfLen)
	if len(other) != selfLen {
		panic("Length not equal...")
	}
	if self.hasValx() || other.hasValx() {
		return res.arithToX()
	}
	resU64 := self.toU64() / other.toU64()
	res.setFromUnsigned64(resU64)
	return res
}

// 如果Vec当中出现 VAL_X则无法进行算数运算. 含有X则返回true
func (self Vec) hasValx() bool {
	for idx := len(self) - 1; idx >= 0; idx-- {
		if VAL_X_FORMAL == self[idx].ToFormal() || VAL_Z_FORMAL == self[idx].ToFormal() {
			return true
		}
	}
	return false
}

// Vec中所有Bit转为VAL_X_FORMAL
func (self Vec) arithToX() Vec {
	selfLen := len(self)
	for i := 0; i < selfLen; i++ {
		self[len(self)-i-1] = VAL_X_FORMAL
	}
	return self
}
