package main

import "fmt"

type FuncLogicSingleInNode struct { // Netlist Node
	NetlistNode
}

func (self *FuncLogicSingleInNode) Eval() Vec {
	switch self.FuncType {
	case N_FUNC_BUF:
		return self.InputVals[0].changeZ2X()
	case N_FUNC_BUFT: // Can receive strength
		return self.InputVals[0]
	case N_FUNC_BUFZ: // Can not receive strength
		return self.InputVals[0] // FIXME: panic if have strength
	case N_FUNC_NOT:
		return self.InputVals[0].bitwiseInvertX()
	default:
		panic("Unsupported func type in logic node.")
	}
}

// Functor nodes have multiple inputs
type FuncLogicSchedulableNode struct { // Netlist Node
	SchedulableNode
}

func (self *FuncLogicSchedulableNode) Eval() Vec {
	inputsLen := len(self.InputVals)
	if inputsLen < 2 {
		panic("Too few inputs.")
	}
	var res_temp Vec
	switch self.FuncType {
	case N_FUNC_OR:
		res_temp = self.InputVals[0]
		for i := 1; i < inputsLen; i++ {
			res_temp = res_temp.bitwiseOr(self.InputVals[i]) // SAFE.  bitwiseOr is not in-situ
		}
		return res_temp
	case N_FUNC_AND:
		res_temp = self.InputVals[0]
		for i := 1; i < inputsLen; i++ {
			res_temp = res_temp.bitwiseAnd(self.InputVals[i])
		}
		return res_temp
	case N_FUNC_NAND:
		res_temp = self.InputVals[0]
		for i := 1; i < inputsLen; i++ {
			res_temp = res_temp.bitwiseNand(self.InputVals[i])
		}
		return res_temp
	case N_FUNC_NOR:
		res_temp = self.InputVals[0]
		for i := 1; i < inputsLen; i++ {
			res_temp = res_temp.bitwiseNor(self.InputVals[i])
		}
		return res_temp
	case N_FUNC_XNOR:
		res_temp = self.InputVals[0]
		for i := 1; i < inputsLen; i++ {
			res_temp = res_temp.bitwiseXnor(self.InputVals[i])
		}
		return res_temp
	case N_FUNC_XOR:
		res_temp = self.InputVals[0]
		for i := 1; i < inputsLen; i++ {
			res_temp = res_temp.bitwiseXor(self.InputVals[i])
		}
		return res_temp
	default:
		panic("Unsupported func type in logic node.")
	}
}

type FuncMuxFrom2Node struct { // Verilog: assign a=b?c:d; VVP: MUXZ
	SchedulableNode
}

// in InputVals:
//InputVals[0]: if0; InputVals[1]: if1; InputVals[2]: condition
func (self *FuncMuxFrom2Node) Eval() Vec {
	//if inputsLen != 3 {
	//	panic("Err, fewer or more than 3 inputs in mux node.")
	//}
	if len(self.InputVals[2]) != 1 {
		fmt.Println("Condition vector of node", self.GetLabelStr(), ":", self.InputVals[2])
		panic("The condition length should be 1.")
	}
	condVal := self.InputVals[2][0]
	if self.InputVals[0].mustEq(self.InputVals[1]) {
		//fmt.Println("cond-mux: 12eq", self.GetLabelStr())
		return self.InputVals[0]
	} else if condVal == VAL_0_FORMAL {
		//fmt.Println("cond-mux: condis0", self.GetLabelStr())
		return self.InputVals[0]
	} else if condVal == VAL_1_FORMAL {
		//fmt.Println("cond-mux: condis1", self.GetLabelStr())
		return self.InputVals[1]
	} else if condVal == VAL_X_FORMAL || condVal == VAL_Z_FORMAL {
		return Vec{VAL_X_FORMAL}
	}

	return Vec{VAL_X_FORMAL}
}

type FuncArithSingleInNode struct {
	NetlistNode
}

func (self *FuncArithSingleInNode) Eval() Vec {
	inputsLen := len(self.InputVals)
	if inputsLen != 1 {
		fmt.Println("Input amount:", inputsLen)
		panic("Input amount should be 1.")
	}
	switch self.FuncType {
	case N_ABS:
		panic("Unsupported.")
	default:
		panic("Unsupported.")
	}
}
